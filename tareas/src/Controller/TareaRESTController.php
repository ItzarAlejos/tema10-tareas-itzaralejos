<?php
/**
 * Created by PhpStorm.
 * User: daw2
 * Date: 21/01/19
 * Time: 15:18
 */

namespace App\Controller;

use App\Entity\Tarea;
use Jenssegers\Date\Date;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Validator\Validator\ValidatorInterface;


/**
 * @Route("/tareas/api")
 */
class TareaRESTController extends FOSRestController
{
    /**
     * @Rest\Get("/",name="pepe")
     */
    public function listTareas()
    {
        $serializer = $this->get('jms_serializer');

        $repositorio = $this->getDoctrine()->getRepository(Tarea::class);
        $tareas = $repositorio->findAll();
        if(count($tareas) > 0){
            $ok=array(
                'ok' => true,
                'tareas' => $tareas
            );
        }else{
            $ok=array(
                'ok' => false,
                'err' => 'No hay tareas'
            );
        }
        return new Response($serializer->serialize($ok,"json"));

    }
    /**
     * @Rest\Get("/{id}",name="paco")
     */
    public function listTareasId($id)
    {
        $serializer = $this->get('jms_serializer');

        $repositorio = $this->getDoctrine()->getRepository(Tarea::class);
        $tareas = $repositorio->find($id);

        if($tareas!=null){
            $ok=array(
                'ok' => true,
                'tareas' => $tareas
            );
        }else{
            $ok=array(
                'ok' => false,
                'err' => 'No hay tareas'
            );
        }
        return new Response($serializer->serialize($ok,"json"));
    }
    /**
     * @Rest\Post("/",name="de")
     */
    public function insertTarea(Request $request,ValidatorInterface $validator)
    {
        $serializer = $this->get('jms_serializer');
        $entityManager=$this->getDoctrine()->getManager();

        $tarea= new Tarea();
        $tarea->setDescripcion($request->get('descripcion'));
        $tarea->setPrioridad($request->get('prioridad'));
        $tarea->setFecha($request->get('fecha'));
        $errores = $validator->validate($tarea);
        if(count($errores) == 0) {
            $tarea->setFecha(Date::createFromFormat('d/m/Y', $request->get('fecha')));
            $entityManager->persist($tarea);
            try {
                $entityManager->flush($tarea);
                $ok = array(
                    'ok' => true,
                    'tareas' => $tarea
                );

            } catch (\Exception $exception) {
                $ok = array(
                    'ok' => false,
                    'err' => $exception
                );
            }
        }else{
            $ok= array(
                'ok' => false,
                'err' => "Error en el formulario, La descripcion y la fecha no pueden estar vacios y la prioridad ha de ser entre 1 y 3 ambos incluidos."
            );
        }
        return new Response($serializer->serialize($ok, "json"));
    }
    /**
     * @Rest\Delete("/{id}", name="mierda")
     */
    public function borrarTarea($id)
    {
        $serializer = $this->get('jms_serializer');
        $entityManager = $this->getDoctrine()->getManager();
        $repositorio = $this->getDoctrine()->getRepository(Tarea::class);
        $tareas= $repositorio->find($id);

        if($tareas){
            $entityManager->remove($tareas);
            $entityManager->flush();
            $ok=array(
                'ok' => true,
                'tareas' => $tareas
            );
        }else{
            $ok=array(
                'ok' => false,
                'err' => 'No hay tareas'
            );
        }
        return new Response($serializer->serialize($ok,"json"));
    }
    /**
     * @Rest\Put("/{id}", name="loco")
     */
    public function putTarea($id,Request $request, ValidatorInterface $validator)
    {
        $serializer = $this->get('jms_serializer');

        $repositorio = $this->getDoctrine()->getRepository(Tarea::class);
        $entityManager= $this->getDoctrine()->getManager();
        $tareas = $repositorio->find($id);

        if($tareas!=null){
            $tareas->setDescripcion($request->get('descripcion'));
            $tareas->setPrioridad($request->get('prioridad'));
            $tareas->setFecha($request->get('fecha'));

            $errores = $validator->validate($tareas);
            if(count($errores) == 0) {
                $tareas->setFecha(Date::createFromFormat('d/m/Y', $request->get('fecha')));
                $entityManager->flush();
                $ok = array(
                    'ok' => true,
                    'tareas' => $tareas
                );
            }else{
                $ok= array(
                    'ok' => false,
                    'err' => "Error en el formulario, La descripcion y la fecha no pueden estar vacios y la prioridad ha de ser entre 1 y 3 ambos incluidos."
                );
            }
        }else{
            $ok=array(
                'ok' => false,
                'err' => 'No hay tareas'
            );
        }
        return new Response($serializer->serialize($ok,"json"));
    }

    /**
     * @Rest\Post("/login", name="login")
     */
    public function login()
    {

    }
}