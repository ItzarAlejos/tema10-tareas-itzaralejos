<?php

namespace App\Controller;

use App\Entity\Tarea;
use Jenssegers\Date\Date;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Created by PhpStorm.
 * User: daw2
 * Date: 18/01/19
 * Time: 16:45
 */

class TareaController extends AbstractController
{
    public function cargarTarea()
    {
        $repositorio = $this->getDoctrine()->getRepository(Tarea::class);
        $tareas = $repositorio->findAll();
        if (count($tareas) > 0)
        {
            $resultado = "";
            foreach ($tareas as $tarea)
                $resultado .= $tarea->getDescripcion() . " (" . $tarea->getFecha()->format("d/m/Y") . ")<br />";

            return new Response($resultado);
        }
        else
        {
            return new Response("No se han encontrado películas");
        }
    }

    public function fechas()
    {
        $repo=$this->getDoctrine()->getRepository(Tarea::class);
        $tareas= $repo->findAll();
        Date::setLocale('es');
        $str="";
        if(count($tareas)>0){
            foreach ($tareas as $tarea){
                $str3=$tarea->getFecha()->format('d/m/Y');
                $fechaEnt = Date::createFromFormat('d/m/Y', $str3);
                $diff= $fechaEnt->timespan();
                $str .= $tarea->getDescripcion() . ': ' . $diff . '<br>';
            }
        }

        return new Response($str);
    }

}